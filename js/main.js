
jQuery(function($){
    "use strict"; 

//Main slider
    if($('.home-slider').length){
        $('.home-slider').slick({
            vertical: true,
            arrows: false,
            dots: true,
            autoplay: true,
            autoplaySpeed: 5000,
        });
    }
   
// shop slider
    if($('.shop-carusel').length){
        $('.shop-carusel').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            appendArrows: $('.shop-slider_nav'),
            prevArrow: '<div class="left"><i class="fas fa-arrow-left"></i></div>', 
            nextArrow: '<div class="right"><i class="fas fa-arrow-right"></i></div>',
            responsive: [              
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }                
              ]                      
          });
    }                                                                                                                   
    
//feedback slider 
if($('.feedback-carousel').length){
        $('.feedback-carousel').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            appendArrows: $('.feedback-slider_box'),
            prevArrow: '<div class="left-arrow"><div class="left"><i class="fas fa-arrow-left"></i></div></div>',
            nextArrow: '<div class="right-arrow"><div class="right"><i class="fas fa-arrow-right"></i></div></div>', 
            responsive: [              
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }                
              ]                      
          });
          $('.feedback-carousel').on('beforeChange', function(event, slick, currentSlide, nextSlide){
            var tabs_container = $('.feedback-container');            

            tabs_container.find($('.tab-item')).hide();
            tabs_container.find($('.tab-' + nextSlide)).show();
          });
    }

//Twitter slider
if($('.content_slider_twitter').length){
    $('.content_slider_twitter').slick({
        vertical: true,
        arrows: true,        
        appendArrows: $('.twitter-slider .arrows'),
        prevArrow: '<span class="up"><i class="fas fa-arrow-up"></i></span>', 
        nextArrow: '<span class="down"><i class="fas fa-arrow-down"></i></span>',      
    });
}    
    
//Pop up video
    if($('.video-box_pop-up').length){
        $('.video-box_pop-up').magnificPopup({
            type: 'iframe'        
          });
    }
    
// Pop up gallery  
    if($('.feedback-popup').length){
    $('.feedback-popup').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1] 
          },        
      });
   }    
});

//mobile menu 
if($('.fa-bars').length){
    $('.fa-bars').on('click', function(){
     $('.mobile-nav').show();
     $(this).hide();
     $('.fa-times').show();     
     });

     $('.fa-times').on('click', function(){
        $('.mobile-nav').hide();
        $(this).hide();
        $('.fa-bars').show();
    });
}
